(** Network module *)

open Unix

(** client type *)
type client = file_descr

(** request from a client: a real query, or, the client disconnected *)
type 'a requete =
    ClientMsg of client * 'a
  | ClientDisconnect of client

exception Network_error

(** server socket *)
let server_socket = socket PF_INET SOCK_STREAM 0

(** client socket *)
let client_socket = socket PF_INET SOCK_STREAM 0

(** get the first available IP for a given hostname *)
let get_ip hostname =
  let host =
    try gethostbyname hostname
    with
    | Not_found ->
      Utils._log ("network: WARNING: can't find address of " ^ hostname ^ ", using local address instead");
      gethostbyname "localhost"
    | Unix_error(e, _, _) ->
      Utils._log ("network: ERROR: " ^ (error_message e));
      raise Network_error
  in
  try host.h_addr_list.(0)
  with Invalid_argument e ->
    Utils._log ("network: ERROR: " ^ e);
    raise Network_error

(** connect to a given host on a given port *)
(* TODO: déconnecter la socket si on était déjà connecté avec
 * sur une autre adresse*)
let connect_to_server host port =
  try
    let host = if host = "" then gethostname () else host in
    let sockaddr = ADDR_INET(get_ip host, port) in
    connect server_socket sockaddr
  with Unix.Unix_error (e, _, _) ->
    let host = if host = "" then "localhost" else host in
    let m = host ^ ":" ^ (string_of_int port) in
    Utils._log ("network: ERROR: " ^ (error_message e));
    Utils._log ("network: ERROR: can't connect to server on " ^ m);
    raise Network_error

(** start a server on a given port *)
let server_start port =
  try
    let localhost = gethostname () in
    let sockaddr = ADDR_INET(get_ip localhost, port) in
    setsockopt server_socket SO_REUSEADDR true;
    bind server_socket sockaddr;
    listen server_socket 3
  with Unix.Unix_error (e, _, _) ->
    Utils._log ("network: ERROR: " ^ (error_message e));
    Utils._log ("network: ERROR: can't start server on localhost:" ^ (string_of_int port));
    exit 1

(** alias for Marshal.header_size *)
let hs = Marshal.header_size

(** alias for Bytes.create hs *)
let header = Bytes.create hs

let rec read_msg_and_next_header descr =
  let len = Marshal.total_size header 0 in
  let buf = Bytes.create (len+hs) in
  Bytes.blit header 0 buf 0 hs;
  let rcv = recv descr buf hs len [] in
  let obj = Marshal.from_bytes buf 0 in
  if rcv < len then [obj]
  else begin
    Bytes.blit buf len header 0 hs;
    obj::(read_msg_and_next_header descr)
  end

(** wait until a client connect to the server *)
let wait_client_connection () =
  let ready, _, _ = select [server_socket] [] [] (-1.0) in
  assert (List.length ready=1);
  fst (accept server_socket)

let empty_msg_queue descr =
  if (recv descr header 0 hs []) < hs then []
  else read_msg_and_next_header descr

(** get request from a list of clients and specify a time passed to select *)
let requests_clients time ls =
  let process_request s =
    let l = empty_msg_queue s in
    if l = [] then [ClientDisconnect(s)]
    else List.map (fun m -> ClientMsg(s, m)) l
  in
  let ready, _, _ = select ls [] [] time in
  List.concat (List.map process_request ready)

(** get request from a list of clients *)
let read_clients_requests = requests_clients 0.1
